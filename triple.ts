export interface Triple<V, E> {
  subject: V
  predicate: E
  object: V
}
