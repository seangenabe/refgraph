# Refgraph

In-memory reference-based graph database / graph data structure.

Inspired by [LevelGraph](https://www.npmjs.com/package/levelgraph).

[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/seangenabe/refgraph.svg?style=flat-square)](https://gitlab.com/seangenabe/refgraph)
[![npm](https://img.shields.io/npm/v/refgraph.svg?style=flat-square)](https://www.npmjs.com/package/refgraph)

## API

```typescript
import { Graph } from "refgraph"

const graph = new Graph<V, E>()
```

### Triple format

A triple is an object containing the keys `subject`, `predicate`, and `object`. Extra fields can be set.

This data structure relies on `Map`-key equality for these keys.

### graph.set(triple)

```typescript
set(triple: Triple): this
```

Inserts a triple into the graph. If a triple with the same subject, predicate, and object already exists in the database, it will be replaced with the new object.

### graph.match(pattern)

Finds matching triples from the graph. The pattern is an object that can have any combination of the keys `subject`, `predicate`, or `object`.

### Updating triples

Since objects are stored by reference, any kind of modification on any part of the triple is ok as long as the values keyed by `subject`, `predicate`, and `object` are not modified. Any modification of these values will result to undefined behavior not in the scope of this library.

### graph.delete(triple)

Deletes a triple from the graph.

### graph.v(vertex)

Creates a new vertex traversal from the specified vertex.

## Graph traversal

Refgraph exposes a simple graph traversal API.

```typescript
const graph = new Graph()
for (let [subject, predicate, object] of [
  ["a", "child", "b"], ["a", "child", "c"]
]) {
  graph.set({ subject, predicate, object })
}

for (let obj of graph
  .v("b").as("first")
  .in("child").as("middle")
  .out("child").as("last")
  .collect()
) {

  obj
  // { first: "b", middle: "a", last: "b" }
  // { first: "b", middle: "a", last: "c" }
}
```

Almost all traversal methods yield a new traversal object that holds immutable state.

For the below method descriptions:
* `in` means traveling towards the current result.
* `out` means traveling away from the current result.
* `both` means getting results from both `in` and `out` directions.
* For the methods with optional parameters: `undefined` can be a valid vertex, so passing `undefined` is not the same as passing no arguments at all. Passing `undefined` will match `undefined` vertices or edges with the graph.

### Common traversal methods

#### traversal.as(varName)

Assigns a key name to the current object being traversed. For a vertex traversal, the object will be a vertex. For an edge traversal, the object will be an edge.

#### traversal.collect()

Collects all of the named objects as a single solution. Emits solution objects.

### Vertex traversal methods

The following methods:

* `out`
* `in`
* `both`

Has overloads:
* `()` - Get all adjacent vertices in the indicated direction
* `(predicate: E)` - Filter by predicate
* `(predicate: E, vertex: V)` - Filter by predicate and vertex

All return `VertexTraversal`.

Has the signature `(predicate?: E) => EdgeTraversal`:

* `outE`
* `inE`
* `bothE`


#### vertexTraversal[Symbol.iterator]()

Iterates through the currently traversed vertices.

### Edge traversal methods

Has the signature `(vertex?) => VertexTraversal`:

* `outV`
* `inV`
* `bothV`

#### edgeTraversal[Symbol.iterator]()

Iterates through the currently traversed edges.
